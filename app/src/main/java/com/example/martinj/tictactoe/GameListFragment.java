package com.example.martinj.tictactoe;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by martinj on 2015-07-07.
 */
public class GameListFragment extends Fragment{

    private ArrayList<String> myStringArray = new ArrayList<String>();
    private ArrayAdapter<String> adapter;
    private int cnt = 1;
    private GameListAdapter customAdater;

    // Create a message handling object as an anonymous class.
    private AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position, long id) {
            // Do something in response to the click
            Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, myStringArray);

        customAdater = new GameListAdapter(getActivity());

        View rootView = inflater.inflate(R.layout.game_list_fragment, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.game_list);
//        listView.setAdapter(adapter);
        listView.setAdapter(customAdater);
        listView.setOnItemClickListener(mMessageClickedHandler);

        customAdater.setItems(myStringArray);

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_new_game) {
//            myStringArray.add("Game " + Integer.toString(cnt));
//            cnt++;
//            adapter.notifyDataSetChanged();

            customAdater.add("Game " + Integer.toString(cnt));
            cnt++;
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
