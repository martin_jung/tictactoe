package com.example.martinj.tictactoe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martinj on 2015-07-08.
 */
public class GameListAdapter extends BaseAdapter {

    private List<String> mItems = new ArrayList<>();
    private final Context mContext;
    private LayoutInflater inflater;

    public GameListAdapter(Context context) {
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void add(String item) {
        mItems.add(item);
        notifyDataSetChanged();
    }

    public void setItems(List<String> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // all of this is about one row of list

        ItemViewHolder viewHolder;

        final int index = position;

        // convertView is the swapView (recycle)
        LinearLayout itemLayout = (LinearLayout) convertView;

        if (itemLayout == null) {
            itemLayout = (LinearLayout) inflater.inflate(R.layout.game_list_item, parent, false);

            viewHolder = new ItemViewHolder();
            viewHolder.title = (TextView) itemLayout.findViewById(R.id.game_title);
            itemLayout.setTag(viewHolder); // set tag for each row
        } else {
            viewHolder = (ItemViewHolder) itemLayout.getTag();
        }

        viewHolder.title.setText((String) getItem(position));

        return itemLayout;
    }

    static class ItemViewHolder {
        TextView title;
    }


}
