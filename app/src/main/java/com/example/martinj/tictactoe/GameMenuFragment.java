package com.example.martinj.tictactoe;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by martinj on 2015-07-07.
 */
public class GameMenuFragment extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.game_menu_fragment, container, false);

        Button button = (Button) rootView.findViewById(R.id.play_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent intent = new Intent(getActivity().getApplicationContext(), GameListActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }
}
