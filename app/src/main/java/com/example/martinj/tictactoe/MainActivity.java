package com.example.martinj.tictactoe;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends Activity {

    private static final String O = "O";
    private static final String X = "X";
    private static final String NO_WINNER = "No Winner";

    private boolean isOTurn = true;

    private char board[][] = new char[3][3];
    private Button[] buttons = new Button[9];
    private TextView o_score, x_score;

    private int oScore = 0;
    private int xScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttons[0] = (Button) findViewById(R.id.b1);
        buttons[1] = (Button) findViewById(R.id.b2);
        buttons[2] = (Button) findViewById(R.id.b3);
        buttons[3] = (Button) findViewById(R.id.b4);
        buttons[4] = (Button) findViewById(R.id.b5);
        buttons[5] = (Button) findViewById(R.id.b6);
        buttons[6] = (Button) findViewById(R.id.b7);
        buttons[7] = (Button) findViewById(R.id.b8);
        buttons[8] = (Button) findViewById(R.id.b9);

        o_score = (TextView) findViewById(R.id.o_score);
        x_score = (TextView) findViewById(R.id.x_score);

        updateScoreBoard();
    }

    public void onClickButton(View view) {
        Button button = (Button) view;
        setButtonText(button);

        switch(view.getId()) {
            case R.id.b1:
                board[0][0] = button.getText().charAt(0);
                break;
            case R.id.b2:
                board[0][1] = button.getText().charAt(0);
                break;
            case R.id.b3:
                board[0][2] = button.getText().charAt(0);
                break;
            case R.id.b4:
                board[1][0] = button.getText().charAt(0);
                break;
            case R.id.b5:
                board[1][1] = button.getText().charAt(0);
                break;
            case R.id.b6:
                board[1][2] = button.getText().charAt(0);
                break;
            case R.id.b7:
                board[2][0] = button.getText().charAt(0);
                break;
            case R.id.b8:
                board[2][1] = button.getText().charAt(0);
                break;
            case R.id.b9:
                board[2][2] = button.getText().charAt(0);
                break;
        }

        checkWinner();

    }

    private void setButtonText(Button b) {
        if ((b.getText() != O) && (b.getText() != X)) {
            b.setText(isOTurn ? O : X);
            isOTurn = !isOTurn;
        }
    }

    private void checkWinner() {
        String winner = NO_WINNER;

        // check rows
        if (winner == NO_WINNER) {
            winner = compareCells(board[0]);
        }
        if (winner == NO_WINNER) {
            winner = compareCells(board[1]);
        }
        if (winner == NO_WINNER) {
            winner = compareCells(board[2]);
        }

        // check columns
        if (winner == NO_WINNER) {
            winner = compareCells(new char[]{board[0][0], board[1][0], board[2][0]});
        }
        if (winner == NO_WINNER) {
            winner = compareCells(new char[]{board[0][1], board[1][1], board[2][1]});
        }
        if (winner == NO_WINNER) {
            winner = compareCells(new char[]{board[0][2], board[1][2], board[2][2]});
        }

        // check diagonal
        if (winner == NO_WINNER) {
            winner = compareCells(new char[]{board[0][0], board[1][1], board[2][2]});
        }
        if (winner == NO_WINNER) {
            winner = compareCells(new char[] {board[0][2], board[1][1], board[2][0]});
        }

        if (winner == O) {
            oScore++;
            Toast.makeText(getApplicationContext(), R.string.o_win, Toast.LENGTH_LONG).show();
            resetGame();
        } else if (winner == X) {
            xScore++;
            Toast.makeText(getApplicationContext(), R.string.x_win, Toast.LENGTH_LONG).show();
            resetGame();
        } else if (isTieGame()) {
            Toast.makeText(getApplicationContext(), R.string.tie, Toast.LENGTH_LONG).show();
            resetGame();
        }
    }

    private String compareCells(char[] cells) {
        if ((cells[0] == cells[1]) && (cells[1] == cells[2])) {
            if (String.valueOf(cells[0]).equals(O)) {
                return O;
            } else if (String.valueOf(cells[0]).equals(X)) {
                return X;
            }
        }
        return NO_WINNER;
    }

    private void resetGame() {
        board = new char[3][3];

//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

        for (Button button : buttons) {
            button.setText(null);
        }

        isOTurn = true;

        updateScoreBoard();
    }

    private boolean isTieGame() {
        for (char[] rows : board) {
            for (char cell : rows) {
                if ((cell != 'O') && (cell != 'X')) {
                    return false;
                }
            }
        }
        return true;
    }

    private void updateScoreBoard () {
        o_score.setText(Integer.toString(oScore));
        x_score.setText(Integer.toString(xScore));
    }

}
